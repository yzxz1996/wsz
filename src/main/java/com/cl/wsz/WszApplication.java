package com.cl.wsz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WszApplication {

    public static void main(String[] args) {
        SpringApplication.run(WszApplication.class, args);
    }

}
