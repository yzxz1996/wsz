package com.cl.wsz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by Leo 2020/07/01
 */
@RestController
@RequestMapping(value = "hello")
public class PigController {

    @GetMapping("linux")
    public String pig(){
        return "我是猪";
    }
}
